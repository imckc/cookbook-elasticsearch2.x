# Uninstallation of plugins
script "uninstall_license" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin remove license
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/license") }
end

script "uninstall_marvel-agent" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin remove marvel-agent
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/marvel-agent") }
end

script "uninstall_shield" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin remove shield
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/shield") }
end

script "uninstall_watcher" do
  interpreter "bash"
  user "root"
  cwd "/tmp"
  code <<-EOH
    #{node[:elasticsearch][:home_dir]}/bin/plugin remove watcher
    EOH
  not_if { File.exist?("#{node[:elasticsearch][:home_dir]}/plugins/watcher") }
end